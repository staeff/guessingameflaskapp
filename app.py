from flask import Flask, render_template, request, session
from random import randint
app = Flask(__name__)
app.secret_key = 'kajfkanfio2po20402of2o3p2'


@app.route('/')
def hello_world():
    answer = randint(0, 20)
    session['answer'] = answer
    return render_template('index.html', error='')


@app.route('/guess', methods=['POST'])
def guess_number():
    answer = session['answer']
    try:
        guess = int(request.form['guess'])
    except:
        return render_template('index.html', error='Enter an integer')

    if guess > answer:
        return render_template('index.html', error='Guess is too high')
    elif guess < answer:
        return render_template('index.html', error='Guess is too low')

    return render_template('success.html')
